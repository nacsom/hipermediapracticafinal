


// INICIALIZAMOS EL CLIENTE DE SOUNDCLOUD
SC.initialize({
    client_id: '9a37602ff892269c59bd179abf0ad916'
});


// CUANDO EL DOCUMENTO ESTÉ READY
jQuery(document).ready(function ($) {


    //Seleccionamos el tema a recomendar
    var number = Math.floor((Math.random() * 10) + 1);
    var recomend = "";
    switch(number) {
    case 1:
        recomend = "house"
        break;
    case 2:
        recomend = "rock"
        break;
    case 3:
        recomend = "festival"
        break;
    case 4:
        recomend = "feat"
        break;
    case 5:
        recomend = "hits"
        break;
    case 6:
        recomend = "party"
        break;
    case 7:
        recomend = "cover"
        break;
    case 8:
        recomend = "opera"
        break;
    case 9:
        recomend = "soundtrack"
        break;
    default:
        recomend = "single"
    }

    /*AL INICIAR LA PÁGINA*/
    if(isEmpty()){
         SC.get('/tracks', {
                // Buscamos en SoundCloud ciertas canciones a recomendar
                q: recomend, limit: '10', order: 'hotness'}).then(function (tracks) {
                //PROCESAMOS LOS DATOS
                console.log(tracks);
                $('#searchResult').append('<h1>Hi-Fi recomienda:</h1>');
                for (var i = 0, len = 7/*tracks.length*/; i < len; i++) {
                    // AÑADIMOS EN EL DIV #searchResult LOS DATOS ENCONTRADOS
                    $('#searchResult').append(
                        '<div class="display_result">'
                        + "<a id='buscadorCanciones' class='search-result' data-url='"
                        + tracks[i].uri
                        + "' artwork_url='"
                        + tracks[i].artwork_url
                        + "' author = '" 
                        + tracks[i].user.username
                        + "'  songtitle = '" 
                        + tracks[i].title
                        + "''><h3>"
                        + tracks[i].title
                        + "</h3>"
                        +"<center><img src='"
                        +tracks[i].artwork_url
                        +"' style='width:200px;height:200px';></center>"
                        +"</a>"
                        + "</div>"
                    );
                }

            });

    }

    // CUANDO ESCRIBAS ALGO EN EL BUSCADOR

    $("#barra_search").keyup(function () {
        if(!$('#searchResult').is(":visible")){
            $('#searchResult').fadeIn();
        }
        //LA VARIABLE searchData VALE EL VALOR DEL INPUT
        var searchData = $("#barra_search").val();
        // LIMPIAMOS EL DIV #searchResult DE RESULTADOS ANTIGUOS.
        $('#searchResult').html('');

        SC.get('/tracks', {
            // BUSCAMOS EN SOUNDCLOUD EL VALOR DE LA VARIABLE searchData
            q: searchData, limit: '10', order: 'hotness'}).then(function (tracks) {
            //PROCESAMOS LOS DATOS
            console.log(tracks);
            for (var i = 0, len = 7/*tracks.length*/; i < len; i++) {
                // AÑADIMOS EN EL DIV #searchResult LOS DATOS ENCONTRADOS
                $('#searchResult').append(
                    '<div class="display_result">'
                    + "<a id='buscadorCanciones' class='search-result' data-url='"
                    + tracks[i].uri
                    + "' artwork_url='"
                    + tracks[i].artwork_url
                    + "' author = '" 
                    + tracks[i].user.username
                    + "'  songtitle = '" 
                    + tracks[i].title
                    + "''>"
                    + tracks[i].title
                    + "</a>"
                    + "</div>"
                );
            }
        });
        return false;
    });
    if( localStorage.length != 0){
        dataload();
        $('.lista-fav').fadeIn();
    }
    if (localStorage.length == 0){
        $('.lista-fav').fadeOut();
    }

    $("#boton,#icon-search").click(function() {
        $('html,body').animate({
                scrollTop: $(".api").offset().top - 160 },
            'slow');
    });
    $('search-result').click(function () {
        this.append(
            '<div class="display_player">'
        );
    });

    $("#icon-home").click(function() {
        $('html, body').animate({scrollTop:0}, 'slow');
    });

    $("#icon-info").click(function() {
        $('html,body').animate({
                scrollTop: $("#footer").offset().top - 5 },
            'slow');
    });
    $("#favicon").on( "click", ".favo-cancion", function() {
        $(this).addClass('active');
    });

    $( document ).on( "click", ".favo-cancion", function() {
        $(this).addClass('active');
    });
    //Cuando el usuario hace clic en una canción, desplegamos el reproductor
    $( document ).on( "click", ".search-result", function() {
        var dataUrl = $(this).attr( "data-url" );
        var albumUrl = $(this).attr( "artwork_url" );
        var authorname = $(this).attr( "author");
        var titlesong = $(this).attr( "songtitle")


        var iframe = document.querySelector('#widget');
        iframe.src = 'http://w.soundcloud.com/player/?url='+dataUrl;
        var widget = SC.Widget(iframe);
        $('#widget').fadeIn();
        $('#searchResult').fadeOut();

        //Album
        var im = "<center><img src='";
        var albumX = im.concat(albumUrl);
        var albumaux = albumX.concat("' style='width:200px;height:200px';></center>");
        //Título
        var titleaux = "<center><h2>"
        var title2 = titleaux.concat(titlesong);
        var tfinal = title2.concat("</h2></center> ");

        //Autor
        var authoraux = "<center><h4>Uploaded by: "
        var author2 = authoraux.concat(authorname);
        var auxfinal = author2.concat("</h4></center> ");
        
        console.log("Title "+titlesong);
        console.log("Uploaded by "+auxfinal );
        var show = albumaux.concat(tfinal);
        var show2 = show.concat(auxfinal);
        console.log(albumaux);
        $('#album').html(show2);
        $('#album').append(
            '<div class="favicon">'
            + "<a id='fav' class='favo-cancion'>"
            + "<span>&#9734</span>"
            + "</a>"
            + "</div>"
        );

    });

    $( document ).on( "click", ".favo-cancion", function() {
        $('.lista-fav').fadeIn();
        datasave();
        dataload();
    });



    function datasave() {
       // var key = $("#buscadorCanciones").attr( "data-url" );
        var key =  { 'data_url': $("#buscadorCanciones").attr( "data-url" ), 'songtitle': $("#buscadorCanciones").attr( "songtitle"), 'artwork_url': $("#buscadorCanciones").attr( "artwork_url" ), 'username': $("#buscadorCanciones").attr( "author" ) };
        var value =  $("#buscadorCanciones").attr( "songtitle" );
        localStorage.setItem(value, JSON.stringify(key));      //******* setItem()
    }

    function dataload() {
        $("#musicRecomendations").empty();
        //
        var rnd = Math.floor((Math.random() * localStorage.length -1) + 1); //Número random para basarnos en una de las canciones escuchadas 
        //
        for(var i = localStorage.length -1 , j = 0; i >= 0 && j <= 10; i--, j++)    //******* length
        {

            var key = localStorage.key(i);
            var value = localStorage.getItem(key);
            var valueParse =  JSON.parse(value);
            console.log('retrievedObject: ', JSON.parse(value));
           $("#musicRecomendations").append(
                '<div class="display_recomendations">'
                + "<a  class='search-result' data-url='"
                + valueParse['data_url']
                + "' artwork_url='"
                + valueParse['artwork_url']
                + "' author = '"
                + valueParse['username']
                + "' title='"
                + valueParse['songtitle']
                +"'>"
                + valueParse['songtitle']
                + "</a>"
                + "<input class ='delete' type='button' value='Remove'>"
                + "</div>"
           );

            $( ".delete" ).click(function() {
                this.setAttribute('key', key);
                localStorage.removeItem($(this).attr('key'));
                dataload();
            });

            if(i == rnd){
                musicrecomender(valueParse['songtitle']); //Buscamos parecidos
            }
        }
    }


    function musicrecomender(SongName){
        var auxiliar = "Remix ";
        var auxiliar2 = auxiliar.concat(SongName);
        
        $('#searchResult').html(""); //Limpiamos
        $('#searchResult').fadeIn(); 
        SC.get('/tracks', {
            // Buscamos en SoundCloud ciertas canciones a recomendar
            q: auxiliar2, limit: '10', order: 'hotness'}).then(function (tracks) {
            //PROCESAMOS LOS DATOS
            $('#searchResult').append('<h1>En función de lo que has escuchado... </h1>');
            for (var i = 0, len = 7/*tracks.length*/; i < len; i++) {
                // AÑADIMOS EN EL DIV #searchResult LOS DATOS ENCONTRADOS
                console.log("ENTO AL BUCLE POR VEZ "+i);
                $('#searchResult').append(
                    '<div class="display_result">'
                    + "<a id='buscadorCanciones' class='search-result' data-url='"
                    + tracks[i].uri
                    + "' artwork_url='"
                    + tracks[i].artwork_url
                    + "' author = '" 
                    + tracks[i].user.username
                    + "'  songtitle = '" 
                    + tracks[i].title
                    + "''><h3>"
                    + tracks[i].title
                    + "</h3>"
                    +"<center><img src='"
                    +tracks[i].artwork_url
                    +"' style='width:200px;height:200px';></center>"
                    +"</a>"
                    + "</div>"
                );
            }

        });
    }

    function isEmpty(){ //Comprobamos si hay canciones guardadas
        var success = 1;
        if(localStorage.length != 0){
            success = 0;
        }
        return success;
    }


    ///// HAY QUE CONSEGUIR QUE SE VEAN DESPUÉS DE BUSCAR UNA CANCIÓN Y LUEGO QUE QUEDE BONITO, ASÍ YA ESTARÁ
    function afterSearch(){
        $("#searchResult").empty();
        //
        var rnd = Math.floor((Math.random() * localStorage.length -1) + 1); //Número random para basarnos en una de las canciones escuchadas 
        //
        for(var i = localStorage.length -1 , j = 0; i > 0 && j <= 10; i--, j++)    //******* length
        {
            var key = localStorage.key(i);
            var value = localStorage.getItem(key);
            var valueParse =  JSON.parse(value);
            //console.log('retrievedObject: ', JSON.parse(value));
            if(i == rnd){
                musicrecomender(valueParse['songtitle']); //Buscamos parecidos
            }

        }
    }

    /////

});
