# README #

Hi-Fi FM es un servicio de streaming de musica que permite a usuarios escuchar archivos de audio digital. Ademas les ofrece la capacidad de organizar dichos archivos en listas de reproduccion según sus preferencias. Hi-Fi FM tiene un motor de busqueda a traves de la api de SoundCloud, funciones de streaming de musica y un sistema de recomendación de musica.

### ¿Para qué es este repositorio? ###

El repositorio contiene una web de una sola página la cual gestiona una API de Soundcloud. 
En esta página podrás buscar y reproducir las canciones que más te gusten e incluso tendrás la posibilidad de hacer una playlist personal.
La página tiene una función de recomendar canciones al inicio de la misma para esos momentos de inconformidad. 


### ¿Cómo puedo ponerlo en marcha? ###

Descarga el repositorio o clona lo en el Sourcetree.
Inicia el repositorio en tu LocalHost 
Disfruta

### ¿Con quien puedo entrar en contacto en caso de error? ###

En el caso de que exista un error o tengas dudas, no dudes en contactar con los admins.